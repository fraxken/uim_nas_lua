logpath = "E:\\Program Files (x86)\\Nimsoft\\probes\\service\\nas\\scripts\\rafale-mode\\rafale-cleanup.log"

-------------------------------------------------------
-- Function de log
-------------------------------------------------------
function log(text)
    local Tfile = file;
    b,nread = Tfile.read(logpath)
    if b == nil then
        Tfile.create(logpath)
    end
    time = timestamp.format( timestamp.now (),"%c" )
    Tfile.write(logpath,time.." - "..text.."\n")
end

-------------------------------------------------------
-- Fonction de visualitation du contenu des tables
-------------------------------------------------------
function tdump(t)
    local function dmp(t, l, k)
        if type(t) == "table" then
            log(string.format("%s%s:", string.rep(" ", l*2), tostring(k)))
            for k, v in pairs(t) do
                dmp(v, l+1, k)
            end
        else
            log(string.format("%s%s:%s", string.rep(" ", l*2), tostring(k), tostring(t)))
        end
    end
    dmp(t, 1, "root")
end


log("------------------------------------------")
log("Supression de la log rafale-mode_capture"
os.remove("E:\\Program Files (x86)\\Nimsoft\\probes\\service\\nas\\scripts\\rafale-mode\\rafale-mode_capture.log")

log("Début de la supression de la database rafale-mode events")

database.open("rafale-mode.db") -- Ouverture de la database!

-------------------------------------------------------
-- Requête qui va effectuer la suppression du **contenu** de la table, elle ré-initialise aussi les IDs.
-------------------------------------------------------
result,rc = database.query("DELETE FROM events")

-------------------------------------------------------
-- On vérifie que la table n'est pas vide
-------------------------------------------------------
if result ~= nil then

    -------------------------------------------------------
    -- On vérifie le return code, 0 = tout est bon , autres = erreur(s)
    -------------------------------------------------------
    if rc == 4 then

        log("Supression de la table events réussie avec succès");

    else

        log("Code retour SQLite erreur => RC : "..tostring(rc))

    end


else

    -- Si elle est vide on log
    log("La table events de rafale-mode semble vide, fin d'éxecution du script.")

end
