-------------------------------------------------------
-- Database information
-------------------------------------------------------
local dbuser = 'sa';
local dbpass = 'cauimsqlprod';
local dbsource = '127.0.0.1,1433';
local dbname = 'checkconfig';

-------------------------------------------------------
-- Variable qui permet de log ou non.
-------------------------------------------------------
local debug = false;
local print = false;

-- Path for the log file.
local logpath = "E:\\Program Files (x86)\\Nimsoft\\probes\\service\\nas\\scripts\\EXT-Conf\\ext-configuration.log";

-------------------------------------------------------
-- Liste des HUBS que l'on ne souhaite pas prendre en compte
-------------------------------------------------------
local exclude_hubs = {
    --CA_UIM_PARKING = true,
    CA_UIM_NT4_W2000 = true,
    CA_UIM_R_PARKING = true,
    CA_UIM_PARKING_LYB = true,
    CA_UIM_INTERNET_GTW = true,
    CA_UIM_MESURE_A = true,
    CA_UIM_MESURE_B = true,
    CA_UIM_UMP = true,
    CA_UIM_HOMOLOGATION = true,
    CA_UIM_LYBERNET = true,
    CA_UIM_INTEGRATION = true,
    CA_UIM_CENTRAL = true
};

-------------------------------------------------------
-- Liste des robots qu'on ne souhaite pas prendre en compte
-------------------------------------------------------
local exclude_robots = {};

-------------------------------------------------------
-- Liste des probes que l'ont souhaite monitorer.
-------------------------------------------------------
local accepted_probes = {
    controller = true,
    cdm = true,
    logmon = true,
    processes = true,
    ntservices = true,
    ntevl = true
};

-------------------------------------------------------
-- Configuration que l'on récupère dans chaque probe
-------------------------------------------------------
local check_probe = {
    cdm = {
        _cpu_alarm = {
            active = true
        },
        _cpu_alarm_error = {
            active = true,
            threshold = true
        },
        _memory = {
            qos_memory_physical_perc = true
        }
    },
    ntevl = {
        _logs = {
            application = true
        }
    }
};

local function a(c,d)if print then print(c)end;d=d or false;if debug or d then local e=file;local f=timestamp;b,nread=e.read(logpath)if b==nil then e.create(logpath)end;time=f.format(f.now(),"%c")e.write(logpath,time.." - "..c.."\n")end end;local g=database.open("Provider=SQLOLEDB;Initial Catalog="..dbname..";Data Source="..dbsource..";User ID="..dbuser..";Password="..dbpass..";Network Library=dbmssocn;Language=us_english")if not g then a("Unable to open database connection!",true)os.exit()end;local function h(i)local j,rc=database.query(i)if#j>0 and rc==0 then return j[1].id end end;local k=pairs;local l=nimbus.request;local gsub=string.gsub;do local m,n=l('hub','gethubs')["hublist"]if m~=nil then for o,p in k(m)do if exclude_hubs[gsub(p.name,'-','_')]==nil then if p.origin~=nil then a('--HUB => '..p.name,true)local q=true;do local result,rc=database.query("SELECT * FROM hubs_list WHERE name LIKE '"..p.name.."'")if#result>0 and rc==0 then local r=result[1]if r.domain~=p.domain or r.ip~=p.ip or r.origin~=p.origin or r.version~=p.version then local o,s=database.query("UPDATE hubs_list SET domain='"..p.domain.."',ip='"..p.ip.."',origin='"..p.origin.."',versions='"..p.version.."' WHERE name LIKE '"..p.name.."'")if s~=0 then a("       HUB "..p.name.." : Update failed",true)end end else local t="INSERT INTO hubs_list(domain,name,ip,origin,versions) "local u="VALUES ('"..p.domain.."','"..p.name.."','"..p.ip.."','"..p.origin.."','"..p.version.."')"local o,v=database.query(t..u)if v~=0 then a("       HUB "..p.name.." : Insert failed",true)q=false end end end;if q then local w=h("SELECT id FROM hubs_list WHERE name LIKE '"..p.name.."'")robotlist,robotlist_rc=l(p.addr,'getrobots')["robotlist"]if robotlist~=nil then for o,x in k(robotlist)do if x.status~=2 then if exclude_robots[gsub(x.name,'-','_')]==nil then a('   ROBOT__NAME : '..x.name)local y=true;do result,rc=database.query("SELECT * FROM robots_list WHERE hubid='"..w.."' AND name LIKE '"..x.name.."'")if#result>0 and rc==0 then local z=result[1]if z.ip~=x.ip or z.origin~=x.origin or z.version~=x.version then local o,s=database.query("UPDATE robots_list SET ip='"..x.ip.."',origin='"..x.origin.."',versions='"..x.version.."' WHERE name LIKE '"..x.name.."'")if s~=0 then a("       => Robot update failed on robot : "..x.name,true)end end else local t="INSERT INTO robots_list(hubid,name,ip,origin,versions) "local u="VALUES ('"..w.."','"..x.name.."','"..x.ip.."','"..x.origin.."','"..x.version.."')"local o,v=database.query(t..u)if v~=0 then a("       => Robot insert failed on robot : "..x.name,true)end end end;if y then local A=h("SELECT id FROM robots_list WHERE name LIKE '"..x.name.."'")probe_list,probe_list_rc=l(x.addr..'/controller','probe_list')if probe_list~=nil then for B,C in k(probe_list)do if accepted_probes[B]then do local D="SELECT * FROM probes_list WHERE robotid='"..A.."' AND name LIKE '"..B.."'"local result,rc=database.query(D)local E=C.pkg_version or"N.A"if C.pkg_version==nil then a("       -- "..B.." version undefined",true)end;if#result>0 and rc==0 then if result[1].active~=C.active or result[1].version~=C.pkg_version then local o,s=database.query("UPDATE probes_list SET active='"..C.active.."',versions='"..E.."' WHERE name LIKE '"..B.."'")if s~=0 then a("       Fail update from probe => "..B,true)end end else local t="INSERT INTO probes_list(robotid,name,active,versions) "local u="VALUES ('"..A.."','"..B.."','"..C.active.."','"..E.."')"local o,v=database.query(t..u)if v~=0 then a("       Failed insert probe => "..B,true)end end end end end else a("   Failed to reach controller => "..x.addr..'/controller',true)end else a("   Failed to get Foreign key from robot => "..x.name)end end else a("   => "..x.addr.." is down!",true)end end else a("   Failed to get robotlist from => "..p.addr,true)end else a("   Failed to get Foreign Key from hub => "..p.name)end else a(p.name.." have no origin !",true)end end end end end;a("=================> Get probes configuration :",true)local F,rc=database.query("SELECT R.id AS robotid,R.name AS robotname,H.name AS hubname,H.domain FROM robots_list AS R LEFT OUTER JOIN hubs_list AS H ON R.hubid = H.id")if F~=nil and rc==0 then for o,G in k(F)do local H="/"..G.domain.."/"..G.hubname.."/"..G.robotname.."/controller"local I,J=database.query("SELECT name,id FROM probes_list WHERE robotid LIKE '"..G.robotid.."'")if#I>0 then for o,C in k(I)do local K=check_probe[C.name]if K~=nil then local L,M;do local N=pds;local O=N.create()N.putString(O,"name",C.name)L,M=l(H,'probe_config_get',O)end;if L~=nil then for P,Q in k(K)do P=gsub(P,"_","/")for R,G in k(L[P])do for S,T in k(Q)do if S==R and T then do local U,V=database.query("SELECT * FROM probes_config WHERE probeid='"..C.id.."' AND section='"..P.."' AND skey='"..R.."'")if U~=nil and V==0 then if U[1].svalue~=G then local o,s=database.query("UPDATE probes_config SET svalue='"..G.."' WHERE id='"..U[1].id.."'")if s~=0 then a("   Probe configuration update failed, probe name : "..C.name,true)end end else local t="INSERT INTO probes_config(probeid,section,skey,svalue) "local u="VALUES ('"..C.id.."','"..P.."','"..R.."','"..G.."')"local o,v=database.query(t..u)if v~=0 then a("   Probe configuration insert failed, probe name : "..C.name,true)end end end end end end end else a(" => failed to get probes configuration on "..H,true)end end end else a("   => No probes find on robot =>  "..G.robotname,true)end end end;database.close()a("=================> Script finish !",true)
