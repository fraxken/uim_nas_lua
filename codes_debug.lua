--Added the error codes function for extra debug
function codes(rc)
    codes = {
        [0]="OK",
        [1]="error",
        [2]="communication error",
        [3]="invalid argument",
        [4]="not found",
        [5]="already defined",
        [6]="permission denied",
        [7]="temporarily out of resources",
        [8]="out of resources",
        [9]="no space left",
        [10]="broken connection",
        [11]="command not found",
        [12]="login failed",
        [13]="SID expired",
        [14]="illegal MAC",
        [15]="illegal SID",
        [16]="Session id for hub is invalid",
        [17]="Expired",
        [18]="No valid license",
        [19]="Invalid license",
        [20]="Illegal license",
        [21]="Invalid operation finv",
        [100]="user error from this value and up"
    }
    if codes[rc] then
        print("The return code is: "..codes[rc].."\n\n")
    else
        print("Undefined return code")
    end
end
