logpath = "E:\\Program Files (x86)\\Nimsoft\\probes\\service\\nas\\scripts\\rafale-mode\\rafale-cleanup.log"
function log(text)
local Tfile = file;
b,nread = Tfile.read(logpath)
if b == nil then
Tfile.create(logpath)
end
time = timestamp.format( timestamp.now (),"%c" )
Tfile.write(logpath,time.." - "..text.."\n")
end
function tdump(t)
local function dmp(t, l, k)
if type(t) == "table" then
log(string.format("%s%s:", string.rep(" ", l*2), tostring(k)))
for k, v in pairs(t) do
dmp(v, l+1, k)
end
else
log(string.format("%s%s:%s", string.rep(" ", l*2), tostring(k), tostring(t)))
end
end
dmp(t, 1, "root")
end
log("------------------------------------------")
log("Supression de la log rafale-mode_capture et de la database events")
os.remove("E:\\Program Files (x86)\\Nimsoft\\probes\\service\\nas\\scripts\\rafale-mode\\rafale-mode_capture.log")
database.open("rafale-mode.db") -- Ouverture de la database!
result,rc = database.query("DELETE FROM events")
if result ~= nil then
if rc == 4 then
log("Supression de la table events réussie avec succès");
else
log("Code retour SQLite erreur => RC : "..tostring(rc))
end
else
log("La table events de rafale-mode semble vide, fin d'éxecution du script.")
end
